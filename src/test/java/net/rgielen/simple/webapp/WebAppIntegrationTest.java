package net.rgielen.simple.webapp;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.Test;

import static org.junit.Assert.*;

public class WebAppIntegrationTest {

    @Test
    public void serviceSaysHello() throws Exception {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:40080/simple-webapp")
                .build();

        Response response = client.newCall(request).execute();
        assertEquals("Hello Stranger\n", response.body().string());
    }

}